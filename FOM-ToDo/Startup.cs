using FOM_ToDo.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;


namespace FOM_ToDo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // F�gt dem Host Container Sevices hinzu. Konfiguriert die Klassen zur Dependency Injection. 
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();

            services.AddServerSideBlazor();

            services.AddDbContext<ItemsDbContext>(options =>
            {
                options.UseSqlite("Data Source = ToDoItems.db");
            });

            services.AddScoped<ItemServices>();

            // Services k�nnen in folgenden scopes eingef�gt werden:
            //      services.AddSingleton<>()       Eine Instanz f�r alle Nutzer (Blazor Server)
            //      services.AddScoped<>()          Eine Instanz pro Nutzer
            //      services.AddTransient<>()       Eine Instanz f�r jede aktive Page
        }


        // Konfiguriert die HTTP Request Pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            //Alle anfragen von http nach https umleiten
            app.UseHttpsRedirection();

            //statischen Dateien wie HTML, CSS, Images und JavaScript bereitstellen
            app.UseStaticFiles();

            //Erm�glicht die witerleitung von Requests zu den verschiedenen ausf�hrbaren Endpunkte der Applikation
            app.UseRouting();

            //Leitet alle Anfragen auf eine Seite um (SPA!). Nach Konvention meist _Host.cshtml, muss nur bei Abweichung angegeben werden.
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
