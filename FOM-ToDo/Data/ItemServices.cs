﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FOM_ToDo.Data;
using Microsoft.EntityFrameworkCore;

namespace FOM_ToDo.Data
{
    public class ItemServices
    {
        
        private readonly ItemsDbContext dbContext;
        
        public ItemServices(ItemsDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        
        public async Task<List<TodoItem>> GetItemAsync()
        {
            return await dbContext.TodoItem.ToListAsync();
        }
        
        public async Task<TodoItem> AddItemAsync(TodoItem todoItem)
        {
            try
            {
                dbContext.TodoItem.Add(todoItem);
                await dbContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
            return todoItem;
        }
        
        public async Task<TodoItem> UpdateItemAsync(TodoItem todoItem)
        {
            try
            {
                var productExist = dbContext.TodoItem.FirstOrDefault(p => p.Id == todoItem.Id);
                if (productExist != null)
                {
                    dbContext.Update(todoItem);
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return todoItem;
        }
       
        public async Task DeleteItemAsync(TodoItem todoItem)
        {
            try
            {
                dbContext.TodoItem.Remove(todoItem);
                await dbContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }
        

    }
}
