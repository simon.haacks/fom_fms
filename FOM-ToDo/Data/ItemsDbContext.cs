﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FOM_ToDo.Data;


namespace FOM_ToDo.Data
{
    public class ItemsDbContext : DbContext
    {
        
        public ItemsDbContext(DbContextOptions<ItemsDbContext> options)
                : base(options)
        {
        }
        
        public DbSet<TodoItem> TodoItem { get; set; }
        
        
    }
}


/*
Add-Migration and Update-Database
Open the Package Manager Console and execute the following two commands:

Add-Migration “Initial-Commit”
This command will create a migration folder into the project hierarchy and add a C# class file containing the migration details.

Update-Database
Will apply the “Initial-Commit” migration and create the database in the projects folder.
*/