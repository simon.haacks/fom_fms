﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FOM_ToDo.Data
{
    public class TodoItem
    {

        public int Id { get; set; }

        public string Text { get; set; }

        public DateTime DoneBy { get; set;}

        public bool Done { get; set; }


        public TodoItem() { }

        public TodoItem(DateTime doneBy)
        {
            this.DoneBy = doneBy;
        }



    }



}
